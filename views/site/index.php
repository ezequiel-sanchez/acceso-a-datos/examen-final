<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="row col-12 jumbotron"> 
        <h1 class="display-4">Enunciados</h1>
    </div>
    
    <div class="row col-12 jumbotron">
        
        <div class="card col-4">
        <h1 class="display-4">Ciclistas</h1>
        <p class="lead">Los ciclistas cuya edad esta entre 20 y 40 años mostrando unicamente el dorsal y el nombre del ciclista.</p>
            <?= GridView::widget([
        'dataProvider' => $dp1,
        'columns' => $campos1
    ]); ?>
        </div>
        
        <div class="card col-4">
        <h1 class="display-4">Etapas</h1>
        <p class="lead">Las etapas circulares mostrando solo el numero de etapa y la longitud de las mismas.</p>
            <?= GridView::widget([
        'dataProvider' => $dp2,
        'columns' => $campos2
    ]); ?>
        </div>
        
        <div class="card col-4">
        <h1 class="display-4">Puertos</h1>
        <p class="lead">Los puertos con altura mayor a 1000 metros figurando el nombre del puerto y el dorsal del ciclista ganador.</p>
            <?= GridView::widget([
        'dataProvider' => $dp3,
        'columns' => $campos3
    ]); ?>
        </div>
        
    </div>






</div>
